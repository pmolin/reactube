import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';
import Video from "./components/Video";
import Channel from "./components/Channel";
import InfosVideo from "./components/InfosVideo";
import InfosChannel from "./components/InfosChannel";
import './App.css';

// router
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
    const [videos, setVideos] = useState([]);
    const [selectedVideo, selectVideo] = useState({});
    const [selectedChannel, selectChannel] = useState({});
    const [selectedType, selectType] = useState([]);


    return (
        <Container className="p-3">
            <Router>
                <MyNav onResults={setVideos} selectedType={selectedType} selectType={selectType}/>

                <Switch>
                    <Route path="/video/:videoId">
                        <InfosVideo id={selectedVideo.id}
                                    snippet={selectedVideo.snippet} player={selectedVideo.player}
                                    statistics={selectedVideo.statistics} />
                    </Route>
                    <Route path="/channel/:channelId">
                        <InfosChannel id={selectedChannel.id}
                                      snippet={selectedChannel.snippet}
                                      statistics={selectedChannel.statistics} />
                    </Route>
                    <Route path="/search/video/:search">
                        <>
                            {videos.map(v => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = v.snippet;

                                return (<Video
                                    videoId={v.id.videoId}
                                    thumbnail={thumbnails.high}
                                    description={description}
                                    channelTitle={channelTitle}
                                    publishTime={publishTime}
                                    title={title}
                                    selectVideo={selectVideo}/>);
                            })}
                        </>
                    </Route>
                    <Route path="/search/channel/:search">
                        <>
                            {videos.map(c => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = c.snippet;

                                return (<Channel
                                    channelId={c.id.channelId}
                                    thumbnail={thumbnails.high}
                                    description={description}
                                    channelTitle={channelTitle}
                                    publishTime={publishTime}
                                    title={title}
                                    selectChannel={selectChannel}/>);
                            })}
                        </>
                    </Route>
                    <Route path="/">
                        <h1>Bienvenue sur Reactube</h1>
                    </Route>
                </Switch>
            </Router>
        </Container>
    );
};

export default App;
