import React from 'react';
import Card from 'react-bootstrap/Card';
import {Col, Image, Row} from 'react-bootstrap';
import api from "../lib/api";
import {useHistory} from "react-router-dom";

const Video = ({videoId, thumbnail, title, description, channelTitle, publishTime, selectVideo, selectedType}) => {
    const history = useHistory();
    const search = async () => {
        const resp = await api.get('/videos', {
            params: {
                id: videoId,
                part: 'snippet,statistics,contentDetails,player,recordingDetails,topicDetails',
            }
        });
        selectVideo(resp.data.items[0]);
        history.push(`/video/${videoId}`);
    };

    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Row>
                <Col xs={3}>
                    <Image src={thumbnail.url}
                           fluid={true}
                           rounded
                           onClick={search} className={"clickable"}/>
                </Col>
                <Col>
                    <Card.Title onClick={search}>{title}</Card.Title>
                    <Card.Subtitle>{channelTitle}</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text className={"mb-1 text-muted"}>
                        {publishTime}
                    </Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>)
};

export default Video;
